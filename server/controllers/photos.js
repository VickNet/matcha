import express from "express";
import passport from "passport";
import multer from "multer";
import cors from "cors";
import path from "path";
import createError from "http-errors";
import { Photo, User } from "../models";

const router = express.Router();
let corsOptions = {
  origin: "http://localhost:4200",
  methods: "POST",
  allowedHeaders: ["X-Requested-With", "Content-Type", "Authorization"],
  credentials: true,
  optionsSuccessStatus: 204,
  preflightContinue: true
};

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, path.join(__dirname, "../public/photos/"));
  },
  filename(req, file, cb) {
    const datetimestamp = Date.now();
    cb(
      null,
      `${file.fieldname}-${datetimestamp}.${file.originalname.split(".")[
        file.originalname.split(".").length - 1
      ]}`
    );
  }
});

const upload = multer({
  storage
}).single("file");

router.options("/upload", cors(corsOptions));

corsOptions = {
  origin: "http://localhost:4200",
  methods: "POST",
  allowedHeaders: ["X-Requested-With", "Content-Type", "Authorization"],
  credentials: true,
  optionsSuccessStatus: 204
};

router
  .post(
    "/profilePhoto",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserById(req.user._id, (err, user) => {
        if (err) return next(createError(500, "Erroare."));
        if (!user) return next(createError(404, "Utilizator inexistent"));
        if (req.body.url === user.photo)
          next(createError(409, "Poza este deja de implicită."));
        user.photo = req.body.url;
        return user.save(error => {
          if (error) throw err;
          res.status(200).send("Poza a fost actualizată.");
        });
      });
    }
  )
  .post(
    "/upload",
    passport.authenticate("jwt", { session: false }),
    cors(corsOptions),
    (req, res, next) => {
      upload(req, res, err => {
        if (err) return next(createError(500, err));
        const newPhoto = new Photo({
          address: `/photos/${req.file.filename}`,
          authorId: req.user.id
        });
        return Photo.addPhoto(newPhoto, error => {
          if (error) return next(createError(500, "Error occured."));
          return res.status(200).send("Success.");
        });
      });
    }
  )
  .delete(
    "/delete/:url",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      if (!req.params.url) return next(createError(400, "URL necesar."));
      return Photo.findOne({ address: req.params.url }, (err, photo) => {
        if (err) return next(createError(400, "Adresa URL este invalidă."));
        if (!photo) return next(createError(404, "Poza nu a fost găsită."));
        if (req.user.id !== photo.authorId)
          return next(createError(403, "Nu sunteți proprietarul acestei poze"));
        return photo.remove(error => {
          if (error) return next(createError(500, error));
          // check if was the profile photo
          return res.status(200).send("Poza a fost ștearsă.");
        });
      });
    }
  )
  .get(
    "/gallery/:id",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      let user = req.user.id;
      if (req.params.id && req.params.id !== "my")
        user = req.params.id;
      Photo.find({ authorId: user })
        .select("address -_id")
        .exec((err, photos) => {
          if (!photos)
            return next(createError(404, "Acest utilizator nu are fotografii"));
          return res.status(200).json(photos);
        });
    }
  );

export default router;
