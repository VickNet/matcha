import express from "express";
import passport from "passport";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import CreateError from "http-errors";
import { User, Alert } from "../models";
import config from "../config/database";

const router = express.Router();

router
  .get(
    "/filled",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      const id = req.user._id;
      User.getUserById(id, (err, user) => {
        if (err) return next(CreateError(500, "Nu se poate gasi utilizatorul"));
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        user.filled =
          user.preferences !== undefined &&
          user.preferences !== "" &&
          user.biography !== undefined &&
          user.biography !== "" &&
          user.location !== undefined &&
          user.location !== "" &&
          user.gender !== undefined &&
          user.gender !== "" &&
          user.photo !== undefined &&
          user.photo !== "" &&
          user.tags !== undefined &&
          user.tags !== "";
        if (!user.filled) return next(CreateError(404, "Completati profilul"));
        return user.save(error => {
          if (error)
            return next(CreateError(500, "Nu putem salva utilizatorul"));
          return res.sendStatus(200);
        });
      });
    }
  )
  .post("/register", (req, res, next) => {
    const newUser = new User({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      gender: req.body.gender,
      username: req.body.username,
      password: req.body.password,
      birthdate: req.body.birthdate
    });
    User.addUser(newUser)
      .then((user, err) => {
        if (err) throw new CreateError(400, `Date incorecte: ${err}`);
        res.status(200).send("User has succesfuly registred");
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/authenticate", (req, res) => {
    res.send("authenticate");
  })
  .post("/authenticate", (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;
    const checked = req.body.checked;

    User.getUserByUsername(username, (err, user) => {
      if (err) throw err;
      if (!user) return next(CreateError(404, "User not found."));

      return User.comparePassword(password, user.password, (error, isMatch) => {
        if (error) throw error;
        if (!isMatch) return next(CreateError(400, "Parolă greșită."));
        const token = jwt.sign(user, config.secret, {
          expiresIn: checked ? 604800 : 86400
        });
        return User.setOnline(user, er => {
          if (er) throw er;
          res.status(200).json({ token: `JWT ${token}`, user });
        });
      });
    });
  })
  .get(
    "/profile",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      res.status(200).json(req.user);
    }
  )
  .put(
    "/updateprofile",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserById(req.user._id, (err, user) => {
        if (err) throw err;

        if (!user)
          return next(CreateError(404, "Utilizatorul nu a fost gasit"));
        user.lastname = req.body.lastname;
        user.username = req.body.username;
        user.firstname = req.body.firstname;
        user.biography = req.body.biography;
        user.preferences = req.body.preferences;
        user.lat = req.body.lat;
        user.lng = req.body.lng;
        if (req.body.city && req.body.country)
          user.location = `${req.body.city}, ${req.body.country}`;
        return user.save(error => {
          if (error)
            if (error.code === 11000)
              return next(CreateError(403, "Acest username deja exista."));
            else return next(CreateError(403, "Oops. Mai încercați."));
          return res.status(200).send("Profilul a fost actualizat.");
        });
      });
    }
  )
  .put(
    "/password",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserById(req.user._id, (err, user) => {
        if (err) throw err;
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        return bcrypt.compare(
          req.body.oldpassword,
          user.password,
          (error, isMatch) => {
            if (error) throw error;
            if (!isMatch) next(CreateError(400, "Parola veche este gresita."));
            bcrypt.genSalt(10, (e, salt) => {
              bcrypt.hash(req.body.newpassword, salt, (er, hash) => {
                if (er) throw er;
                user.password = hash;
                user.save(errors => {
                  if (errors) return next(CreateError(500, "Eroare."));
                  return res
                    .status(200)
                    .send("Parola a fost schimbata cu succes.");
                });
              });
            });
          }
        );
      });
    }
  )
  .put(
    "/email",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserById(req.user._id, (err, user) => {
        if (err) throw err;
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        return bcrypt.compare(
          req.body.currentpassowrd,
          user.password,
          (error, isMatch) => {
            if (error) throw error;
            if (!isMatch)
              return next(CreateError(400, "Parola veche este gresita."));
            user.email = req.body.newemail;
            return user.save(errors => {
              if (errors)
                if (errors.code === 11000)
                  return next(CreateError(403, "E-mail-ul este deja în uz."));
                else return next(CreateError(403, "Oops. Mai încercați."));
              return res
                .status(200)
                .send("E-mailul a fost schimbat cu succes.");
            });
          }
        );
      });
    }
  )
  .put(
    "/tag",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserByUsername(req.user.username, (err, user) => {
        if (err) throw err;
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        let tags = [];
        if (user.tags !== undefined) tags = user.tags.split(",");
        tags.push(req.body.tag);
        user.tags = tags.toString();
        return user.save(error => {
          if (error) return next(CreateError(500, "Eroare."));
          return res
            .status(200)
            .send(`Tag-ul "${req.body.tag}" a fost adăugat.`);
        });
      });
    }
  )
  .get(
    "/validate/:username",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserByUsername(req.params.username, (err, user) => {
        if (err) throw err;
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        if (user.filled) {
          Alert.findBlock(user._id, req.user._id, (eror, blocat) => {
            if (eror) throw eror;
            if (blocat) {
              return res.sendStatus(403);
            } else {
              let blocked = false;
              Alert.findBlock(req.user._id, user._id, (error, block) => {
                if (error) throw error;
                if (block) blocked = true;
                const profile = {
                  id: user._id,
                  lastname: user.lastname,
                  firstname: user.firstname,
                  biography: user.biography,
                  gender: user.gender,
                  photo: user.photo,
                  birthdate: user.birthdate,
                  location: user.location,
                  preferences: user.preferences,
                  tags: user.tags,
                  online: user.online,
                  block: blocked
                };
                return res.status(200).json(profile);
              });
            }
          });
        } else {
          return res.sendStatus(403);
        }
      });
    }
  )
  .delete(
    "/tag/:tag",
    passport.authenticate("jwt", { session: false }),
    (req, res, next) => {
      User.getUserByUsername(req.user.username, (err, user) => {
        if (err) throw err;
        if (!user) return next(CreateError(404, "Utilizator inexistent"));
        if (!user.tags || user.tags === "")
          return next(CreateError(404, "Nu sunt taguri la acest utilizator"));
        let tags = [];
        tags = user.tags.split(",");
        tags.splice(tags.indexOf(req.params.tag), 1);
        user.tags = tags.toString();
        return user.save(error => {
          if (error) return next(CreateError(500, "Eroare."));
          return res.status(200).send(`Tag-ul "${req.params.tag}" a fost eliminat.`);
        });
      });
    }
  )
  .get("/tags/:username", (req, res, next) => {
    User.getUserByUsername(req.params.username, (err, user) => {
      if (err) throw err;
      if (!user) return next(CreateError(404, "Utilizator inexistent"));
      return res.status(200).json(user.tags);
    });
  })
  .get("/all", (req, res) => {
    User.find({}, (err, users) => {
      res.status(200).json(
        users.map(user => ({
          id: user._id,
          firstname: user.firstname,
          lastname: user.lastname,
          username: user.username,
          name: user.name,
          email: user.email,
          biography: user.biography,
          gender: user.gender,
          photo: user.photo,
          location: user.location,
          birthdate: user.birthdate,
          preferences: user.preferences,
          online: user.online
        }))
      );
    });
  });

export default router;
