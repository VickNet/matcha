import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { AuthService } from "./auth.service";
import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";

import * as io from "socket.io-client";

@Injectable()
export class SocketService {
  private url = "http://localhost:8085/";
  public socket = io(this.url);
  connection;
  auth;

  constructor(private authService: AuthService, private http: Http) {}

  setOnline() {
    console.log("Conectare...");
    if (localStorage.user) {
      const id = JSON.parse(localStorage.user)._id;
      this.socket.emit("log-in", id);
    }
  }

  setOffline() {
    console.log("Deconecare...");
    if (localStorage.user) {
      const id = JSON.parse(localStorage.user)._id;
      this.socket.emit("log-out", id);
    }
  }

  sendMessage(message, id) {
    const data = {
      message: message,
      from: JSON.parse(localStorage.user)._id,
      to: id
    };
    this.socket.emit("add-message", data);
  }

  seen(id) {
    this.socket.emit('seen', id);
  }

  getMessages() {
    const observable = new Observable(observer => {
      this.socket.on("message", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getNotification() {
    const observable = new Observable(observer => {
      this.socket.on("notification", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getUserOnline(id) {
    this.socket.emit("isActive", id);
  }

  getUserDetails() {
    const observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on("user details", profile => {
        observer.next(profile);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getChaters() {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("notifications/chaters");
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }
}
