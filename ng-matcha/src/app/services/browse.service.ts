import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class BrowseService {
  auth: any;

  constructor(private authService: AuthService, private http: Http) {}

  getAllUsers() {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("users/all");
    return this.http.get(ep).map(res => res.json());
  }

  getAllPhotos(user) {
    user = user ? user : "my";
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`photos/gallery/${user}`);
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }

  deletePhoto(url) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`photos/delete/${url}`);
    return this.http.delete(ep, { headers: headers });
  }

  profilePhoto(url) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("photos/profilePhoto");
    return this.http.post(ep, { url }, { headers: headers });
  }

  like(to, url) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/like");
    return this.http
      .post(ep, { to, url }, { headers: headers })
      .map(res => res);
  }

  block(id) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/block");
    return this.http.post(ep, { id }, { headers: headers }).map(res => res);
  }

  report(id) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/report");
    return this.http.post(ep, { id }, { headers: headers }).map(res => res);
  }

  getLikes(to) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`alerts/like/${to}`);
    return this.http.get(ep, { headers: headers }).map(res => res.json());
  }

  guest(to) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/guest");
    return this.http.post(ep, { to }, { headers: headers });
  }

  unlike(url) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`alerts/like/${url}`);
    return this.http.delete(ep, { headers: headers }).map(res => res);
  }

  unblock(id) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`alerts/block/${id}`);
    return this.http.delete(ep, { headers: headers });
  }

  checkUser(username) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`users/validate/${username}`);
    return this.http.get(ep, { headers: headers });
  }

  getFameRating(username) {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint(`alerts/faim/${username}`);
    return this.http.get(ep, { headers: headers });
  }
}
