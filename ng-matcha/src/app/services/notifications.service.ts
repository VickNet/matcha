import { Http, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class NotificationsService {
  auth: any;

  constructor(private authService: AuthService, private http: Http) {}

  getNotifications() {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/notifications");
    return this.http.get(ep, {headers}).map(res => res.json());
  }

  getActivity() {
    const headers = new Headers();
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    headers.append("Authorization", this.auth);
    headers.append("Content-Type", "application/json");
    const ep = this.authService.prepEndpoint("alerts/activites");
    return this.http.get(ep, {headers}).map(res => res.json());
  }

}