import {Http, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class SettingsService {

  user: any;

  constructor(private authService:AuthService,
              private http:Http) { }

  changePassword(user, newpassword, oldpassword) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type','application/json');
    let ep = this.authService.prepEndpoint('users/password');
    return this.http.put(ep, { user, newpassword, oldpassword }, {headers: headers})
      .map(res => res.json());
  }

  addTag(tag, nickname)
  {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type','application/json');
    let ep = this.authService.prepEndpoint('users/tag');
    return this.http.put(ep, { tag, nickname }, {headers: headers})
      .map(res => res.json());
  }

  removeTag(tag, nickname)
  {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type','application/json');
    let ep = this.authService.prepEndpoint(`users/tag/${tag}`);
    return this.http.delete(ep, {headers: headers})
      .map(res => res.json());
  }

  changeEmail(user, currentpassowrd, newemail) {
    let headers = new Headers();
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type','application/json');
    let ep = this.authService.prepEndpoint('users/email');
    return this.http.put(ep, { user, currentpassowrd, newemail }, {headers: headers})
      .map(res => res.json());
  }

}
