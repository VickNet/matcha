import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

/* Plugins */
import { FileUploadModule } from "ng2-file-upload";
import { ContextMenuModule } from "ngx-contextmenu";
import { GooglePlaceModule } from "ng2-google-place-autocomplete";
import { FileSelectDirective, FileDropDirective } from "ng2-file-upload";

/* Main Component */
import { AppComponent } from "./app.component";

/* User Components */
import { LoginComponent } from "./components/user/login/login.component";
import { RegisterComponent } from "./components/user/register/register.component";

import { ProfilesModule } from "./components/user/profiles/profiles.module";
import { InfoGeneralComponent } from "./components/user/settings/info-general.component";
import { TagsComponent } from "./components/user/settings/tags.component";
import { ChangeEmailComponent } from "./components/user/settings/change-email.component";
import { ChangePasswordComponent } from "./components/user/settings/change-password.component";
import { UploadComponent } from "./components/user/upload/upload.component";
import { EditComponent } from "./components/user/edit/edit.component";

import { NotificationsComponent } from './components/user/notifications/notifications.component';

/* Website components */
import { HomeComponent } from "./components/website/home/home.component";
import { NavbarComponent } from "./components/website/navbar/navbar.component";
import { FooterComponent } from "./components/website/footer/footer.component";

/* Services */
import { AuthService } from "./services/auth.service";
import { BrowseService } from "./services/browse.service";
import { SocketService } from "./services/socket.service";
import { SettingsService } from "./services/settings.service";
import { ValidateService } from "./services/validate.service";
import { MessageService } from "./services/message.service";
import { NotificationsService } from "./services/notifications.service";

/* Guards */
import { AuthGuard } from "./guards/auth.guard";
import { FilledGuard } from "./guards/filled.guard";
import { NotAuthGuard } from "./guards/notAuth.guard";

/* Modules */
import {ToastyModule} from 'ng2-toasty';
import { MaterializeModule } from "angular2-materialize";
import { ChatModule } from "./components/user/chat/chat.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const appRoutes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "register",
    component: RegisterComponent,
    canActivate: [NotAuthGuard]
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [NotAuthGuard]
  },
  {
    path: "settings",
    component: EditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "notifications",
    component: NotificationsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TagsComponent,
    EditComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    UploadComponent,
    RegisterComponent,
    ChangeEmailComponent,
    InfoGeneralComponent,
    NotificationsComponent,
    ChangePasswordComponent
  ],
  imports: [
    HttpModule,
    ChatModule,
    FormsModule,
    BrowserModule,
    ProfilesModule,
    FileUploadModule,
    MaterializeModule,
    GooglePlaceModule,
    ContextMenuModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
    AuthService,
    FilledGuard,
    NotAuthGuard,
    SocketService,
    BrowseService,
    MessageService,
    ValidateService,
    SettingsService,
    NotificationsService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
