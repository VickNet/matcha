import "rxjs/add/operator/switchMap";
import { Observable } from "rxjs/Observable";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";

import { Profile, ProfileService } from "./profile.service";

@Component({
  templateUrl: "./profile-list.component.html",
  styleUrls: ["./profile-list.component.css"]
})
export class ProfileListComponent implements OnInit {

  params = [{
    edge:'right',
    draggable: true
  }]

  profiles: Observable<Profile[]>;

  private selectedId: String;

  constructor(
    private service: ProfileService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.profiles = this.route.params.switchMap((params: Params) => {
      this.selectedId = params["username"];
      return this.service.getProfiles();
    });
  }
  
  onSelect(profile: Profile) {
    this.router.navigate(["/profile", profile.username]);
  }
}
