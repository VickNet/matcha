import { ChatService } from "./chat.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";

import { ChatComponent } from "./chat.component";

import { SocketService } from "../../../services/socket.service";
import { MessageService } from "../../../services/message.service";

import * as $ from 'jquery';
import * as Materialize from 'angular2-materialize';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: "app-chat",
  templateUrl: "./messages.component.html",
  styleUrls: ["./messages.component.css", "./spinner.css"]
})
export class MessagesComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  chat_id;
  message;
  fullname;
  loading = true;

  messages = [];
    
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ChatService,
    private socket: SocketService,
    private messageService: MessageService
  ) { }

  add_message(message) {
    this.messages.push({
      message: message,
      me: false,
      date: Date.now()
    })
    //scroll();
  }

  ngOnInit() {
    let self = this;
    this.route.params
    .subscribe(params => {
      this.chat_id = params["id"];
      this.service.getCheater(this.chat_id).subscribe(
        data => {
        this.fullname = `${data.firstname} ${data.lastname} (${data.username})`;
        this.subscription = this.messageService.messageObservable$.subscribe(data => {
          this.add_message(data["message"]);
          this.socket.seen(data["_id"]);
        })
        this.service.getMessages(this.chat_id).subscribe(messages => {
          for(let message of messages){
            self.messages.unshift({
              message: message["message"],
              me: !(this.chat_id == message["author"]),
              date: message["date"]
            })
          }
          this.loading = false;
        })
      },
      error => {
        Materialize.toast('Alege de aici!', 2000, 'blue');
        this.router.navigate(['/chat']);
      }
    )
    })
  }

  ngOnDestroy() {
    if(this.subscription)
      this.subscription.unsubscribe();
  }
  
  scroll() {
    /* $("#chat").scrollTop($("#chat")[0].scrollHeight); */
  }

  keyDownFunction(event) {
    if(event.keyCode == 13 && this.message != undefined && this.message != "")
      this.onSend();
}

  onSend(){
    this.messages.push({
        message: this.message,
        me: true,
        date: Date.now()
    })
    //scroll();
    this.socket.sendMessage(this.message, this.chat_id);
    this.message = "";
  }
}
