import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { ChatComponent } from "./chat.component";
import { MessagesComponent } from "./messages.component";

import { ChatService } from "./chat.service";

import { ChatRoutingModule } from "./chat-routing.module";

@NgModule({
  imports: [CommonModule, FormsModule, ChatRoutingModule],
  declarations: [ChatComponent, MessagesComponent],
  providers: [ChatService]
})
export class ChatModule {}
