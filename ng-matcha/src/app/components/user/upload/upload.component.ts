import { AuthService } from "../../../services/auth.service";
import { BrowseService } from "../../../services/browse.service";
import { MaterializeAction } from "angular2-materialize";
import { Component, EventEmitter, ViewChild, OnInit } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { ContextMenuComponent } from "ngx-contextmenu";
import { FileUploader } from "ng2-file-upload";

import * as Materialize from "angular2-materialize";

@Component({
  selector: "my-upload",
  templateUrl: "upload.component.html",
  styleUrls: ["upload.component.css"]
})
export class UploadComponent implements OnInit {
  username: String;
  countrent: number;
  imageURLs: Array<String>;
  auth: any;
  user: any;
  options: Object;
  public uploader: FileUploader;
  public hasBaseDropZoneOver = false;

  @ViewChild("carousel") carouselElement;
  actions = new EventEmitter<string>();
  showInitialized = false;
  constructor(
    private authService: AuthService,
    private http: Http,
    private browseService: BrowseService
  ) {}

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  setAsProfile(url) {
    console.log(this.user);
    this.browseService.profilePhoto(url).subscribe(data => {
      if (data.ok) {
        this.user.photo = url;
        Materialize.toast(data.text(), 666);
      }
    });
  }

  delete(url) {
    url = `/photos/${url.split("/")[url.split("/").length - 1]}`;
    const self = this;
    this.browseService.deletePhoto(url).subscribe(data => {
      if (data.ok) {
        self.loadPhotos(null);
      }
      Materialize.toast(data.text(), 666);
    });
  }

  ngOnInit() {
    this.authService.getProfile().subscribe(
      profile => {
        this.user = profile;
      },
      err => {
        console.error(err);
        return false;
      }
    );
    this.authService.loadToken();
    this.auth = this.authService.authToken;
    this.options = {
      url: this.authService.prepEndpoint("photos/upload"),
      allowedMimeType: ["image/png", "image/jpeg"],
      maxFileSize: 2500 * 2500,
      autoUpload: true,
      headers: [{ name: "Authorization", value: this.auth }]
    };
    this.uploader = new FileUploader(this.options);
    this.uploader.onCompleteItem = (
      item: any,
      response: any,
      status: any,
      headers: any
    ) => {
      if (status === 200) {
        Materialize.toast(response, 666);
        this.loadPhotos(null);
      } else {
        Materialize.toast(`A aparut o eroare la incarcare: ${response}`, 666);
      }
    };
    this.loadPhotos(null);
  }

  loadPhotos(user) {
    const self = this;
    this.browseService.getAllPhotos(user).subscribe(images => {
      self.imageURLs = images.map(image =>
        this.authService.prepEndpoint(
          image.address.slice(1, image.address.length)
        )
      );
      self.countrent = self.imageURLs.length;
    });
  }
}
