import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { SettingsService } from '../../../services/settings.service';
import { ValidateService } from '../../../services/validate.service';

import * as Materialize from "angular2-materialize";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  password:   String;
  repeat:     String;
  old:        String;
  valid:      boolean = true;
  user:       any;

  constructor(private validateService: ValidateService,
              private settingsService:     SettingsService,
              private router:          Router,
              private authService:     AuthService) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
    }, err => {
      console.error(err);
      return false;
    })
  }

  onChangePassword() {
    if(!this.password || !this.old || !this.repeat){
      this.valid = false
      Materialize.toast("Completați toate câmpurile.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }
    else if(!this.validateService.validatePassword(this.password)){
      this.valid = false
      Materialize.toast("Parola trebuie să conțină cel puțin o cifră și o literă.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }
    else if(!this.validateService.validatePasswords(this.password, this.repeat)){
      this.valid = false
      Materialize.toast("Parolele nu coincid.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }
    else if(this.validateService.validatePasswords(this.password, this.old))
    {
      this.valid = false
      Materialize.toast("Parola veche coincide cu cea nouă.", 1300, '',()=> {
        this.valid = true
      })
      return false
    }

    this.settingsService.changePassword(this.user,this.password,this.old).subscribe(data => {
      if(data.success) {
          this.authService.logout();
          Materialize.toast(data.msg, 1000,'',
          this.router.navigate(['/login']))
      } else {
          Materialize.toast(data.msg, 1500)
      }

    })
  }

}
