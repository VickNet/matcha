import { Component, OnInit } from "@angular/core";
import { ValidateService } from "../../../services/validate.service";
import { AuthService } from "../../../services/auth.service";
import { Router } from "@angular/router";
import * as Materialize from "angular2-materialize";
import { SocketService } from "../../../services/socket.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  username: String;
  password: String;
  checked: boolean;
  valid: boolean;

  constructor(
    private validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    private socketService: SocketService
  ) {}

  ngOnInit() {
    this.valid = true;
    this.checked = false;
  }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password,
      checked: this.checked
    };
    if (!this.validateService.validateLogin(user)) {
      this.valid = false;
      Materialize.toast(
        "Vă rugăm să completați toate câmpurile.",
        2000,
        "",
        () => {
          this.valid = true;
        }
      );
      return false;
    } else if (!this.validateService.validatePassword(user.password)) {
      this.valid = false;
      Materialize.toast("Parola este incorectă.", 2000, "", () => {
        this.valid = true;
      });
      return false;
    }

    this.authService.authenticateUser(user).subscribe(
      data => {
        if (data.ok) {
          this.valid = false;
          const body = JSON.parse(data.text());
          this.authService.storeUserData(body.token, body.user);
          this.socketService.setOnline();
          window.location.href = '';
        }
      },
      err => {
        this.valid = false;
        Materialize.toast(err.text(), 1000, "", () => {
          this.router.navigate(["login"]);
          this.valid = true;
        });
      }
    );
  }
}
