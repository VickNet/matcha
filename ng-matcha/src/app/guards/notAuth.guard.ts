import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { AuthService } from "../services/auth.service";

import * as Materialize from "angular2-materialize";

@Injectable()
export class NotAuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate() {
    if (this.authService.loggedIn()) {
      Materialize.toast("Sunteți autentificat.", 1900);
      this.router.navigate(["/"]);
      return false;
    } else {
      return true;
    }
  }
}
